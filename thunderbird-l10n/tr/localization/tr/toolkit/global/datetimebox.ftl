# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Temizle

## Placeholders for date and time inputs

datetime-year-placeholder = yyyy
datetime-month-placeholder = aa
datetime-day-placeholder = gg
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Yıl
datetime-month =
    .aria-label = Ay
datetime-day =
    .aria-label = Gün

## Field labels for input type=time

datetime-hour =
    .aria-label = Saat
datetime-minute =
    .aria-label = Dakika
datetime-second =
    .aria-label = Saniye
datetime-millisecond =
    .aria-label = Milisaniye
datetime-dayperiod =
    .aria-label = ÖÖ/ÖS
