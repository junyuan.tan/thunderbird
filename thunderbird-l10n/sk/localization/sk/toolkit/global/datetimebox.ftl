# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Vymazať

## Placeholders for date and time inputs

datetime-year-placeholder = rrrr
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Rok
datetime-month =
    .aria-label = Mesiac
datetime-day =
    .aria-label = Deň

## Field labels for input type=time

datetime-hour =
    .aria-label = Hodiny
datetime-minute =
    .aria-label = Minúty
datetime-second =
    .aria-label = Sekundy
datetime-millisecond =
    .aria-label = Milisekundy
datetime-dayperiod =
    .aria-label = doobeda/poobede
