# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Obraz v obraze

pictureinpicture-pause =
    .aria-label = Pozastaviť
pictureinpicture-play =
    .aria-label = Prehrať

pictureinpicture-mute =
    .aria-label = Stlmiť
pictureinpicture-unmute =
    .aria-label = Zapnúť zvuk

pictureinpicture-unpip =
    .aria-label = Odoslať späť na kartu

pictureinpicture-close =
    .aria-label = Zavrieť

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pozastaviť
    .title = Pozastaviť (medzerník)
pictureinpicture-play-cmd =
    .aria-label = Prehrať
    .title = Prehrať (medzerník)

pictureinpicture-mute-cmd =
    .aria-label = Stlmiť
    .title = Stlmiť ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Zrušiť stlmenie
    .title = Zrušiť stlmenie ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Odoslať späť na kartu
    .title = Späť na kartu

pictureinpicture-close-cmd =
    .aria-label = Zavrieť
    .title = Zavrieť ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Titulky
    .title = Titulky

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Na celú obrazovku
    .title = Na celú obrazovku (dvojité kliknutie)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Ukončiť režim celej obrazovky
    .title = Ukončiť režim celej obrazovky (dvojité kliknutie)

pictureinpicture-seekbackward-cmd =
    .aria-label = Dozadu
    .title = Dozadu (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Dopredu
    .title = Dopredu (→)

pictureinpicture-subtitles-label = Titulky

pictureinpicture-font-size-label = Veľkosť písma

pictureinpicture-font-size-small = Malé

pictureinpicture-font-size-medium = Stredné

pictureinpicture-font-size-large = Veľké
