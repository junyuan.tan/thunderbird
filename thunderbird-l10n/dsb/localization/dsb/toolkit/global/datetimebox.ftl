# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Wuprozniś

## Placeholders for date and time inputs

datetime-year-placeholder = llll
datetime-month-placeholder = mm
datetime-day-placeholder = źź
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Lěto
datetime-month =
    .aria-label = Mjasec
datetime-day =
    .aria-label = Źeń

## Field labels for input type=time

datetime-hour =
    .aria-label = Góźiny
datetime-minute =
    .aria-label = Minuty
datetime-second =
    .aria-label = Sekundy
datetime-millisecond =
    .aria-label = Milisekundy
datetime-dayperiod =
    .aria-label = AM/PM
