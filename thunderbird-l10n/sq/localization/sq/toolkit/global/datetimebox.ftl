# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = Spastroje

## Placeholders for date and time inputs

datetime-year-placeholder = yyyy
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = Vit
datetime-month =
    .aria-label = Muaj
datetime-day =
    .aria-label = Ditë

## Field labels for input type=time

datetime-hour =
    .aria-label = Orë
datetime-minute =
    .aria-label = Minuta
datetime-second =
    .aria-label = Sekonda
datetime-millisecond =
    .aria-label = Milisekonda
datetime-dayperiod =
    .aria-label = AM/PM
