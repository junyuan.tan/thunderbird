# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = ภาพที่เล่นควบคู่

pictureinpicture-pause =
    .aria-label = หยุดชั่วคราว
pictureinpicture-play =
    .aria-label = เล่น

pictureinpicture-mute =
    .aria-label = ปิดเสียง
pictureinpicture-unmute =
    .aria-label = เลิกปิดเสียง

pictureinpicture-unpip =
    .aria-label = ส่งกลับไปที่แท็บ

pictureinpicture-close =
    .aria-label = ปิด

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = หยุดชั่วคราว
    .title = หยุดชั่วคราว (Spacebar)
pictureinpicture-play-cmd =
    .aria-label = เล่น
    .title = เล่น (Spacebar)

pictureinpicture-mute-cmd =
    .aria-label = ปิดเสียง
    .title = ปิดเสียง ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = เลิกปิดเสียง
    .title = เลิกปิดเสียง ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = ส่งกลับไปที่แท็บ
    .title = กลับไปที่แท็บ

pictureinpicture-close-cmd =
    .aria-label = ปิด
    .title = ปิด ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = คำบรรยาย
    .title = คำบรรยาย

##

pictureinpicture-fullscreen-cmd =
    .aria-label = เต็มหน้าจอ
    .title = เต็มหน้าจอ (คลิกสองครั้ง)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = ออกจากภาพเต็มหน้าจอ
    .title = ออกจากภาพเต็มหน้าจอ (คลิกสองครั้ง)

pictureinpicture-seekbackward-cmd =
    .aria-label = ย้อนหลัง
    .title = ย้อนหลัง (←)

pictureinpicture-seekforward-cmd =
    .aria-label = เดินหน้า
    .title = เดินหน้า (→)

pictureinpicture-subtitles-label = คำบรรยาย

pictureinpicture-font-size-label = ขนาดแบบอักษร

pictureinpicture-font-size-small = เล็ก

pictureinpicture-font-size-medium = ปานกลาง

pictureinpicture-font-size-large = ใหญ่
