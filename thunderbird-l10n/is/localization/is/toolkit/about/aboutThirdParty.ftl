# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

third-party-tag-shellex = Skeljarviðbót
    .title = Þessi gerð einingar hleðst inn þegar þú opnar kerfisskráagluggann.

third-party-status-loaded = Hlaðið
third-party-status-blocked = Lokað á
third-party-status-redirected = Endurbeint

third-party-button-copy-to-clipboard = Afrita hrá gögn á klippispjald
third-party-button-reload = Endurhlaða með kerfisupplýsingum
    .title = Endurhlaða með kerfisupplýsingum
