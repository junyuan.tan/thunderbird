# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = ਸਾਫ਼ ਕਰੋ

## Placeholders for date and time inputs

datetime-year-placeholder = yyyy
datetime-month-placeholder = mm
datetime-day-placeholder = dd
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = ਸਾਲ
datetime-month =
    .aria-label = ਮਹੀਨਾ
datetime-day =
    .aria-label = ਦਿਨ

## Field labels for input type=time

datetime-hour =
    .aria-label = ਘੰਟੇ
datetime-minute =
    .aria-label = ਮਿੰਟ
datetime-second =
    .aria-label = ਸਕਿੰਟ
datetime-millisecond =
    .aria-label = ਮਿਲੀਸਕਿੰਟ
datetime-dayperiod =
    .aria-label = ਸਵੇਰ/ਸ਼ਾਮ
