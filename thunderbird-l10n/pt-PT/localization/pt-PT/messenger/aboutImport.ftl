# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar

export-page-title = Exportar

## Header

import-start = Ferramenta de importação

import-start-title = Importe definições ou dados de uma aplicação ou ficheiro.

import-start-description = Selecione a fonte a partir da qual quer importar. Mais tarde será pedido que escolha que dados precisam de ser importados.

import-from-app = Importar de aplicação

import-file = Importar de um ficheiro

import-file-title = Selecione um ficheiro para importar o seu conteúdo.

import-file-description = Escolha para importar de uma cópia anterior de um perfil, livros de endereços ou calendários.

import-address-book-title = Importar ficheiro de Livro de Endereços

import-calendar-title = Importar ficheiro de Calendário

export-profile = Exportar

## Buttons

button-back = Retroceder

button-continue = Continuar

button-export = Exportar

button-finish = Concluir

## Import from app steps

app-name-thunderbird = Thunderbird

app-name-seamonkey = SeaMonkey

app-name-outlook = Outlook

app-name-becky = Becky! Internet Mail

app-name-apple-mail = Correio Apple

source-thunderbird = Importar de outra instalação do { app-name-thunderbird }

source-thunderbird-description = Importar configurações, filtros, mensagens, e outros dados de um perfil do { app-name-thunderbird }

source-seamonkey = Importar de uma instalação do { app-name-seamonkey }

source-seamonkey-description = Importar definições, filtros, mensagens e outros dados de um perfil do { app-name-seamonkey }.

source-outlook = Importar do { app-name-outlook }

source-outlook-description = Importar contas, livros de endereço e mensagens do { app-name-outlook }.

source-becky = Importar do { app-name-becky }

source-becky-description = Importar livros de endereços e mensagens do { app-name-becky }.

source-apple-mail = Importar do { app-name-apple-mail }

source-apple-mail-description = Importe mensagens do { app-name-apple-mail }.

source-file2 = Importar de um ficheiro

source-file-description = Selecionar um ficheiro para importar livros de endereços, calendários ou uma cópia de segurança de perfil (ficheiro ZIP).

## Import from file selections

file-calendar = Importar calendários

## Import from app profile steps

from-app-thunderbird = Importar de um perfil do { app-name-thunderbird }

from-app-seamonkey = Importar de um perfil do { app-name-seamonkey }

from-app-outlook = Importar do { app-name-outlook }

from-app-becky = Importar do { app-name-becky }

from-app-apple-mail = Importar do { app-name-apple-mail }

profiles-pane-title-outlook = Importar dados do { app-name-outlook }

profile-source = Importar do perfil

profile-file-picker-directory = Escolher uma pasta de perfil

items-pane-directory = Diretório:

items-pane-checkbox-address-books = Livros de endereços

items-pane-checkbox-calendars = Calendários

items-pane-checkbox-mail-messages = Mensagens eletrónicas

## Import from address book file steps


## Import from calendar file steps


## Import dialog

error-pane-title = Erro

## <csv-field-map> element


## Export tab

export-brand-name = { -brand-product-name }

## Summary pane


## Footer area


## Step navigation on top of the wizard pages

