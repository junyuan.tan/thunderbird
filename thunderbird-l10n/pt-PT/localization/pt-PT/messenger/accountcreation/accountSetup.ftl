# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

account-setup-tab-title = Configuração da conta

## Header


## Form fields


## Action buttons

account-setup-button-continue = Continuar
    .accesskey = C

account-setup-button-done = Feito
    .accesskey = F

## Notifications


## Illustrations


## Results area


## Error messages


## Manual configuration area

account-setup-protocol-label = Protocolo:

account-setup-hostname-label = Nome do servidor:

account-setup-port-label = Porta:
    .title = Defina o número da porta para 0 para deteção automática

## Incoming/Outgoing SSL Authentication options

ssl-autodetect-option = Deteção automática

ssl-no-authentication-option = Sem autenticação

ssl-cleartext-password-option = Palavra-passe normal

ssl-encrypted-password-option = Palavra-passe encriptada

## Incoming/Outgoing SSL options

ssl-noencryption-option = Nenhuma

account-setup-auth-label = Método de autenticação

account-setup-username-label = Nome de utilizador:

## Warning insecure server dialog


## Warning Exchange confirmation dialog


## Dismiss account creation dialog


## Alert dialogs


## Addon installation section


## Success view


## Calendar synchronization dialog

calendar-dialog-cancel-button = Cancelar
    .accesskey = C

account-setup-calendar-name-label = Nome

