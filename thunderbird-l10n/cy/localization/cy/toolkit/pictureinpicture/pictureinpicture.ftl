# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Llun mewn Llun

pictureinpicture-pause =
    .aria-label = Oedi
pictureinpicture-play =
    .aria-label = Chwarae

pictureinpicture-mute =
    .aria-label = Tewi
pictureinpicture-unmute =
    .aria-label = Dad dewi

pictureinpicture-unpip =
    .aria-label = Anfon nôl i'r tab

pictureinpicture-close =
    .aria-label = Cau

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Oedi
    .title = Oedi (Bar Bylchu)
pictureinpicture-play-cmd =
    .aria-label = Chwarae
    .title = Chwarae (Bar Bylchu)

pictureinpicture-mute-cmd =
    .aria-label = Tewi
    .title = Tewi ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Dad-dewi
    .title = Dad-dewi ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Anfon nôl i'r tab
    .title = Nôl i'r tab

pictureinpicture-close-cmd =
    .aria-label = Cau
    .title = Cau ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Is-deitlau
    .title = Is-deitlau

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Sgrin Lawn
    .title = Sgrin Lawn (clic dwbl)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Gadael sgrin lawn
    .title = Gadael sgrin lawn (clic dwbl)

pictureinpicture-seekbackward-cmd =
    .aria-label = Mynd yn ôl
    .title = Mynd yn ôl (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Mynd Ymlaen
    .title = Mynd Ymlaen (→)

pictureinpicture-subtitles-label = Is-deitlau

pictureinpicture-font-size-label = Maint ffont

pictureinpicture-font-size-small = Bach

pictureinpicture-font-size-medium = Canolig

pictureinpicture-font-size-large = Mawr
