# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Wobraz-we-wobrazu

pictureinpicture-pause =
    .aria-label = Přestawka
pictureinpicture-play =
    .aria-label = Wothrać

pictureinpicture-mute =
    .aria-label = Bjez zynka
pictureinpicture-unmute =
    .aria-label = Ze zynkom

pictureinpicture-unpip =
    .aria-label = K rajtarkej wróćo pósłać

pictureinpicture-close =
    .aria-label = Začinić

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Přestawka
    .title = Přestawka (wobłukowa lajsta)
pictureinpicture-play-cmd =
    .aria-label = Wothrać
    .title = Wothrać (wobłukowe lajsta)

pictureinpicture-mute-cmd =
    .aria-label = Bjez zynka
    .title = Bjez zynka ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Ze zynkom
    .title = Ze zynkom ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = K rajtarkej wróćo pósłać
    .title = Wróćo k rajtarkej

pictureinpicture-close-cmd =
    .aria-label = Začinić
    .title = Začinić ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Podtitule
    .title = Podtitule

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Połna wobrazowka
    .title = Połna wobrazowka (dwójne kliknjenje)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Połnu wobrazowku wopušćić
    .title = Połnu wobrazowku wopušćić (dwójne kliknjenje)

pictureinpicture-seekbackward-cmd =
    .aria-label = Dozady
    .title = Dozady (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Doprědka
    .title = Doprědka (→)

pictureinpicture-subtitles-label = Podtitule

pictureinpicture-font-size-label = Pismowa wulkosć

pictureinpicture-font-size-small = Mały

pictureinpicture-font-size-medium = Srjedźny

pictureinpicture-font-size-large = Wulki
