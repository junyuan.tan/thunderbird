# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Maletg-en-maletg
pictureinpicture-pause =
    .aria-label = Pausa
pictureinpicture-play =
    .aria-label = Far ir
pictureinpicture-mute =
    .aria-label = Senza tun
pictureinpicture-unmute =
    .aria-label = Cun tun
pictureinpicture-unpip =
    .aria-label = Trametter enavos al tab
pictureinpicture-close =
    .aria-label = Serrar

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Pausa
    .title = Pausa (spaziunader)
pictureinpicture-play-cmd =
    .aria-label = Far ir
    .title = Far ir (spaziunader)
pictureinpicture-mute-cmd =
    .aria-label = Senza tun
    .title = Senza tun ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Cun tun
    .title = Cun tun ({ $shortcut })
pictureinpicture-unpip-cmd =
    .aria-label = Trametter enavos al tab
    .title = Enavos al tab
pictureinpicture-close-cmd =
    .aria-label = Serrar
    .title = Serrar ({ $shortcut })
pictureinpicture-subtitles-cmd =
    .aria-label = Suttitels
    .title = Suttitels

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Maletg entir
    .title = Maletg entir (clic dubel)
pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Bandunar il modus da maletg entir
    .title = Bandunar il modus da maletg entir (clic dubel)
pictureinpicture-seekbackward-cmd =
    .aria-label = Enavos
    .title = Enavos (←)
pictureinpicture-seekforward-cmd =
    .aria-label = Enavant
    .title = Enavant (→)
pictureinpicture-subtitles-label = Suttitels
pictureinpicture-font-size-label = Grondezza da scrittira
pictureinpicture-font-size-small = Pitschna
pictureinpicture-font-size-medium = Mesauna
pictureinpicture-font-size-large = Gronda
