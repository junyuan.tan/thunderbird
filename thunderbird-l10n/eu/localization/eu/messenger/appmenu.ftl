# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## New Account

## New Account / Address Book

## Create

## Open

## View / Layout

appmenu-font-size-value = Letra-tamaina

appmenu-mail-uidensity-value = Dentsitatea

appmenuitem-font-size-enlarge =
    .tooltiptext = Handiagotu letra-tipoaren tamaina

appmenuitem-font-size-reduce =
    .tooltiptext = Txikitu letra-tipoaren tamaina

# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size }px
    .tooltiptext = Berrezarri letra-tipo tamaina

## Tools

appmenu-tools-panel-title =
    .title = Tresnak

appmenu-tools-panel =
    .label = Tresnak
    .accesskey = T

appmenu-tools-import =
    .label = Inportatu
    .accesskey = I

appmenu-tools-export =
    .label = Esportatu
    .accesskey = E

appmenu-tools-message-search =
    .label = Bilatu mezuak
    .accesskey = B

appmenu-tools-message-filters =
    .label = Mezuen iragazkiak
    .accesskey = z

appmenu-tools-download-manager =
    .label = Deskargen kudeatzailea
    .accesskey = D

appmenu-tools-activity-manager =
    .label = Jarduera-kudeatzailea
    .accesskey = J

appmenu-tools-dev-tools =
    .label = Garatzaile-tresnak
    .accesskey = G

## Help

appmenu-help-panel-title =
    .title = Laguntza

appmenu-help-get-help =
    .label = Lortu laguntza
    .accesskey = L

appmenu-help-explore-features =
    .label = Eginbideak esploratu
    .accesskey = g

appmenu-help-shortcuts =
    .label = Teklatuaren lasterbideak
    .accesskey = k

appmenu-help-get-involved =
    .label = Parte hartu
    .accesskey = P

appmenu-help-donation =
    .label = Egin dohaintza
    .accesskey = d

appmenu-help-enter-troubleshoot-mode2 =
    .label = Arazoak konpontzeko modua…
    .accesskey = A

appmenu-help-exit-troubleshoot-mode2 =
    .label = Desaktibatu arazoak konpontzeko modua
    .accesskey = D

appmenu-help-troubleshooting-info =
    .label = Arazoak konpontzeko informazioa…
    .accesskey = i

appmenu-help-about-product =
    .label = { -brand-short-name }(r)i buruz
    .accesskey = b
