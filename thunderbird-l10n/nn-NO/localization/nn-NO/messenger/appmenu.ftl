# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## New Account


## New Account / Address Book


## Create


## Open


## View / Layout

appmenu-font-size-value = Skriftstorleik

appmenuitem-font-size-enlarge =
    .tooltiptext = Auk skriftstorleiken

appmenuitem-font-size-reduce =
    .tooltiptext = Reduser skriftstorleiken

# Variables:
# $size (String) - The current font size.
appmenuitem-font-size-reset =
    .label = { $size }px
    .tooltiptext = Tilbakestill skriftstorleiken

## Tools


## Help

appmenu-help-share-feedback =
    .label = Del idear og tilbakemeldingar
    .accesskey = D

