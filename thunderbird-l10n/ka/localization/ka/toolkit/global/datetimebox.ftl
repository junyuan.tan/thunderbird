# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Date/time clear button
datetime-reset =
    .aria-label = გასუფთავება

## Placeholders for date and time inputs

datetime-year-placeholder = წწწწ
datetime-month-placeholder = თთ
datetime-day-placeholder = რრ
datetime-time-placeholder = --

## Field labels for input type=date

datetime-year =
    .aria-label = წელი
datetime-month =
    .aria-label = თვე
datetime-day =
    .aria-label = დღე

## Field labels for input type=time

datetime-hour =
    .aria-label = საათი
datetime-minute =
    .aria-label = წუთი
datetime-second =
    .aria-label = წამი
datetime-millisecond =
    .aria-label = მილიწამი
datetime-dayperiod =
    .aria-label = AM/PM
