# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

pictureinpicture-player-title = Зображення в зображенні

pictureinpicture-pause =
    .aria-label = Пауза
pictureinpicture-play =
    .aria-label = Відтворити

pictureinpicture-mute =
    .aria-label = Вимкнути звук
pictureinpicture-unmute =
    .aria-label = Увімкнути звук

pictureinpicture-unpip =
    .aria-label = Відправити назад у вкладку

pictureinpicture-close =
    .aria-label = Закрити

## Variables:
##   $shortcut (String) - Keyboard shortcut to execute the command.

pictureinpicture-pause-cmd =
    .aria-label = Пауза
    .title = Пауза (Пробіл)
pictureinpicture-play-cmd =
    .aria-label = Відтворити
    .title = Відтворити (Пробіл)

pictureinpicture-mute-cmd =
    .aria-label = Вимкнути звук
    .title = Вимкнути звук ({ $shortcut })
pictureinpicture-unmute-cmd =
    .aria-label = Увімкнути звук
    .title = Увімкнути звук ({ $shortcut })

pictureinpicture-unpip-cmd =
    .aria-label = Відправити назад у вкладку
    .title = Назад у вкладку

pictureinpicture-close-cmd =
    .aria-label = Закрити
    .title = Закрити ({ $shortcut })

pictureinpicture-subtitles-cmd =
    .aria-label = Субтитри
    .title = Субтитри

##

pictureinpicture-fullscreen-cmd =
    .aria-label = Повноекранний режим
    .title = Повноекранний режим (подвійне натискання)

pictureinpicture-exit-fullscreen-cmd =
    .aria-label = Вийти з повноекранного режиму
    .title = Вийти з повноекранного режиму (подвійне натискання)

pictureinpicture-seekbackward-cmd =
    .aria-label = Назад
    .title = Назад (←)

pictureinpicture-seekforward-cmd =
    .aria-label = Уперед
    .title = Уперед (→)

pictureinpicture-subtitles-label = Субтитри

pictureinpicture-font-size-label = Розмір шрифту

pictureinpicture-font-size-small = Маленький

pictureinpicture-font-size-medium = Середній

pictureinpicture-font-size-large = Великий
